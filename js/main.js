import Glasses from "./glasses.js";
import dataGlasses from "./../data/data.js";

const listGlasses = dataGlasses;
console.log(listGlasses);

const getEle = (id) => document.getElementById(id);

// Tạo giao diện ListGlasses

const showListGlasses = (data) => {
  let contentListGlasses = "";
  data.forEach((glasses) => {
    contentListGlasses += `
        <div class="col-4">
            <div class="vglasses__items" onclick="showGlassesInfo('${glasses.id}')">
                <img src="${glasses.src}" alt="" />
            </div>
        </div>
        `;
  });
  // console.log(contentListGlasses);
  getEle("vglassesList").innerHTML = contentListGlasses;
};
showListGlasses(listGlasses);

//Glasses Info
const showGlassesInfo = (id) => {
  let contentGlasses = "";
  let contentGlassesInfo = "";
  for (let i = 0; i < listGlasses.length; i++) {
    if (listGlasses[i].id === id) {
      contentGlasses += `<img src="${listGlasses[i].virtualImg}" alt="" />`;
      contentGlassesInfo += `
                <h5>${listGlasses[i].name} - ${listGlasses[i].brand} (${listGlasses[i].color})</h5>
                <div class="price-status">
                    <span>$${listGlasses[i].price}</span>
                    <p>Stocking</p>
                </div>
                <span
                >${listGlasses[i].description}</span>
            `;
      console.log(contentGlasses);
      console.log(contentGlassesInfo);
    }
  }
  getEle("avatar").innerHTML = contentGlasses;
  getEle("glassesInfo").innerHTML = contentGlassesInfo;
};

window.showGlassesInfo = showGlassesInfo;
